# Nestore Mobile Application
Developed with [Ionic Framework](https://ionicframework.com/), you will find in this repository the code source of the mobile application of NESTORE, which it the main way for users to communicate with the NESTORE system. It includes, among others things, the communication with NESTORE chatbot.

## Version
Tested with :
 - node v10.16.0
 - npm 6.9.0
 - cordova 9.0.0 (cordova-lib@9.0.1)
 - ionic 4.2.1 (included in package.json)


## Development Environment
It is adviced to use [WebStorm](https://www.jetbrains.com/webstorm/).
  - Install [WebStorm](https://www.jetbrains.com/webstorm/)
 
Once installed :
  - Open Webstorm
  - Click on 'Check out from Version Control' -> Git
  <br/>
  <img src="https://git.nestore-coach.eu/heia-fr/nestore-mobile/raw/notification/README/open_webstorm_printscreen.png" width="300">
  - Configure repository as on printscreen below.
  <br/>
  <img src="https://git.nestore-coach.eu/heia-fr/nestore-mobile/raw/notification/README/configure_repository_printscreen.png" width="700">
  - Credentials are required, enter your mail and password. Warning : the password is not the same that for the connection on GitLab platform. Please setup a Personal Access Token (go to https://git.nestore-coach.eu/profile/personal_access_tokens and create one) and use that token value as password (and save the value in a safe place as you won't be able to see it again on UI!).
  <br/>
  <img src="https://git.nestore-coach.eu/heia-fr/nestore-mobile/raw/notification/README/credentials_printscreen.png" width="400">

### Commands
```bash
npm install
```

Read [pre-requisites for ionic documentation](https://ionicframework.com/docs/intro/deploying/)
```bash 
//debug mode - (only on android)
ionic cordova run android
```
The first execution will take time due to installations of plugins. 
A [problem of compatibility](https://github.com/fechanique/cordova-plugin-fcm/issues/481
), will give you an error for the plugin 'cordova-plugin-fcm-with-dependency-updated'. Once it happened, edit the file plugins/cordova-plugin-fcm/scripts/fcm_config_files_process.js and comment lines 44 and 80 with 'process.stdout.write(err);'. Then relaunch the command

If this [second problem](https://stackoverflow.com/questions/49208772/error-resource-androidattr-fontvariationsettings-not-found) of compatibility occurs,
open platforms/android/app/build.gradle and add :
```bash.
configurations.all {
        resolutionStrategy {
                force 'com.android.support:support-v4:27.1.1'
    }
} 
```

```bash.
ionic cordova run android
```

### Current state of the system
![alt text](https://git.nestore-coach.eu/heia-fr/chat-engine/raw/master/README/status.png)

