import {Component, EventEmitter, Input, Output} from '@angular/core';
import {$$} from "@angular/compiler/src/chars";

@Component({
  selector: 'nestore-toggle',
  templateUrl: 'nestore-toggle.component.html',
})
export class NestoreToggleComponent {
  @Input() enabled = true;
  @Input() disabled = false;
  @Output() enabledChange = new EventEmitter();

  change(){
    this.enabledChange.emit(this.enabled);
  }

}
