import {Component, Input} from '@angular/core';

@Component({
  selector: 'nestore-spinner',
  templateUrl: 'spinner.component.html',
})
export class SpinnerComponent {
  @Input() color = '#F58018';
  @Input() size = 'big';

}
