import {Component, EventEmitter, Input, Output} from '@angular/core';
import {GlobalvarProvider} from "../../app/globalprovider";
import {Subject} from "rxjs";
import {TutorialService} from "../../providers/tutorialService";

const IMAGES = {
  pathway: {
    on: "../../assets/icon/pathway_Charts_on.svg",
    off: "../../assets/icon/pathway_Charts_off.svg"
  },
  social: {
    on: "../../assets/icon/Social_Charts_on.svg",
    off: "../../assets/icon/Social_Charts_off.svg"
  },
  physical: {
    on: "../../assets/icon/Physical_Charts_on.svg",
    off: "../../assets/icon/Physical_Charts_off.svg"
  },
  nutrition: {
    on: "../../assets/icon/Food_charts_on.svg",
    off: "../../assets/icon/Food_charts_off.svg"
  },
  cognitive: {
    on: "../../assets/icon/Cognitive_charts_on.svg",
    off: "../../assets/icon/Cognitive_charts_off.svg"
  }
};

@Component({
  selector: 'nestore-filter-header',
  templateUrl: 'nestore-filter-header.component.html',
})
export class NestoreFilterHeaderComponent {

  @Input() timeFrame;
  @Output() timeFrameChange = new EventEmitter();

  @Input() activeDomain;
  @Output() activeDomainChange = new EventEmitter();

  @Input() date;
  @Output() dateChange = new EventEmitter();

  @Output() dateClicked = new EventEmitter();
  @Output() dataChanged = new EventEmitter();

  @Input() hasPathway = false;

  clickNutrition = new Subject();
  clickNutrition2 = new Subject();
  clickNutrition3 = new Subject();
  clickNutrition4 = new Subject();

  weekDates = [null, null]; // contains the first day and the last day of the week
  timeFrameSelection = false;

  allowOutside = true;

  images = {
    pathway: IMAGES.pathway.off,
    social: IMAGES.social.off,
    physical: IMAGES.physical.off,
    nutrition: IMAGES.nutrition.off,
    cognitive: IMAGES.cognitive.off
  };



  ngOnChanges(changes){
    if(changes.activeDomain){
      if(changes.previous==null) {
        this.setActive(this.activeDomain);
      }
    }
    if(changes.date){
      this.computeWeekDates();
    }

    if(changes.activeDomain || changes.date || changes.timeFrame){
      this.dataChanged.emit()
    }
  }

  constructor(public globalvarProvider : GlobalvarProvider, private tutorialService: TutorialService){
    this.clickNutrition.subscribe(()=>{
      this.setActive("nutrition")
    });

    this.clickNutrition2.subscribe((fromTuto)=>{
      if(!fromTuto) {
        this.switchDate('back');
      }
    });

    this.clickNutrition3.subscribe(()=>{
      this.selectDate()
    });

    this.clickNutrition4.subscribe(()=>{
      if(this.tutorialService.showTuto) {
        this.allowOutside = false;
        this.timeFrameSelection=true
      }
      else{
        this.timeFrameSelection=!this.timeFrameSelection;
      }
    })

  }

  changeTimeFrame() {
    this.timeFrameChange.emit(this.timeFrame);
    this.computeWeekDates();
  }

  setActive(d: string) {
    let current = this.activeDomain;
    console.log(current);
    this.activeDomain = d;

    for(let i of Object.keys(this.images)){
      this.images[i] = IMAGES[i].off;
    }
    this.images[d] = IMAGES[d].on;

    this.activeDomainChange.emit(this.activeDomain)
    //this.dataChanged.emit();
  }

  switchDate(direction) {
    if (direction == "back") {
      switch (this.timeFrame) {
        case "day":
          this.date.setDate(this.date.getDate() - 1);
          break;
        case "week":
          this.date.setDate(this.date.getDate() - 7);
          break;
        case "month":
          this.date.setMonth(this.date.getMonth() - 1);
          break;
      }
    }
    else {
      switch (this.timeFrame) {
        case "day":
          this.date.setDate(this.date.getDate() + 1);
          break;
        case "week":
          this.date.setDate(this.date.getDate() + 7);
          break;
        case "month":
          this.date.setMonth(this.date.getMonth() + 1);
          break;
      }
    }

    // make sure the change is reflected to the UI
    this.date = new Date(this.date);

    this.computeWeekDates();
    this.dateChange.emit(this.date);
    //this.dataChanged.emit();
    console.log(this.date);
  }

  selectDate() {
    this.dateClicked.emit();
  }

  computeWeekDates(){
    this.weekDates[0] = this.getMonday(this.date);
    this.weekDates[1] = this.getSunday(this.date);
  }

  getMonday(d) {
    d = new Date(d);
    let day = d.getDay();
    let diff = d.getDate() - day + (day === 0 ? -6 : 1); // adjust when day is monday
    return new Date(d.setDate(diff));
  }

  getSunday(d) {
    d = new Date(d);
    let day = d.getDay();
    let diff = d.getDate() + (6 - day) + (day === 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }

  clickOutside(){
    if(this.allowOutside) {
      this.timeFrameSelection = false;
    }
    else{
      this.allowOutside = true;
    }
  }
}

