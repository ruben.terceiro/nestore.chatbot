import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'nestore-scale',
  templateUrl: 'nestore-scale.component.html',
})
export class NestoreScaleComponent {
  @Output() valid = new EventEmitter();
  @Input() params;
  scale = [];

  ngOnChanges(changes){
    if(changes.params){
      console.log(this.params);
      let from = this.params.min;
      let to = this.params.max;
      let row = [];
      for(let i = from;i<= to;i++){
        console.log(i);
        row.push(i);
      }
      this.scale = row;
    }
  }

  validate(number){
    this.valid.emit(number);
  }
}
