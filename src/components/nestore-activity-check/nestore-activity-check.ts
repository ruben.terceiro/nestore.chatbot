import {Component, Input, ViewChild} from '@angular/core';
import * as d3 from 'd3';


@Component({
  selector: 'nestore-activity-check',
  templateUrl: 'nestore-activity-check.html'
})
export class NestoreActivityCheckComponent {
  @Input() color = 'orange';

  constructor() {
  }

  ngOnInit() {

  }
}
