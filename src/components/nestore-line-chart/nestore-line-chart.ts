import {ChangeDetectorRef, Component, Input} from '@angular/core';
import * as d3 from 'd3';
import {DatePipeProxy} from "../../pipes/DatePipeProxy";
import {TranslatePipe, TranslateService} from "@ngx-translate/core";
import {CoordinatesChartOld} from "../coordinatesChart";


@Component({
  selector: 'nestore-line-chart',
  templateUrl: 'nestore-line-chart.html'
})
export class NestoreLineChartComponent extends CoordinatesChartOld{
  @Input() thresold = 3;
  @Input() label = '3L';
  @Input() pointSize = 5;
  @Input() showPoints = true ;
  @Input() clickable = false;
  @Input() unit;
  displayValue;

  line1;
  line2;
  line3;
  line;
  labelPosition = 0;

  offset = 5;
  maxVar;

  constructor(cdRef:ChangeDetectorRef, private translate: TranslateService) {
    super(cdRef);

  }

ngOnChanges(changes){
    if(!this.max){
      this.maxVar = null;
    }
    else{
      this.maxVar = this.max;
    }
}

  initChart(){

    if(this.line){
      this.line.remove();
    }
    if(this.line2){
      this.line2.remove();
    }
    if(this.line3){
      this.line3.remove();
    }
    if(this.line1){
      this.line1.remove();
    }

    this.max = this.max?this.max:1.3*Math.max.apply( null, this.data );

    super.initChart();

    if(this.thresold){
      this.offset2 = 50;
    }


    let width = this.svgChart.nativeElement.clientWidth;
    let height = width / 4;

    this.labelPosition = height*(3/5);

    if(this.thresold){
      this.labelPosition = height-(height)*(this.thresold/this.max);

    }

    console.log(this.data);
    let allSVGData = this.calculateSVGData( this.data.map((v)=>{return {value: v}}), width-this.offset2-30, height );
    let svgData = allSVGData.filter((coord)=>{return coord[1]!==null});
    console.log(svgData);
    let lineData = "";
    svgData.map(function( coordinates, i ){
        let command = i === 0 ? "M" : "L";
        lineData = lineData
          + " "
          + command
          + " "
          + coordinates[0]
          + ","
          + coordinates[1]
    });


    this.line3 = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "path"
    );

    if(this.thresold) {
      this.line3.setAttribute("d", "M30," + this.labelPosition + " L" + (width - 30) + ", " + this.labelPosition);
      this.line3.setAttribute("fill", "none");
      this.line3.setAttribute("stroke", "black");
      this.line3.setAttribute("stroke-width", "2");
      this.svgChart.nativeElement.prepend(this.line3);


      this.line1 = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "path"
      );
      this.line1.setAttribute("d", "M0," + this.labelPosition + " L" + width + ", " + this.labelPosition);
      this.line1.setAttribute("fill", "none");
      this.line1.setAttribute("stroke", "#F0F0F0");
      this.line1.setAttribute("stroke-width", "2");
      this.svgChart.nativeElement.prepend(this.line1);
    }


    this.line = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "path"
    );
    this.line.setAttribute("d", this.getLineSVG(svgData));
    this.line.setAttribute("fill", "none");
    this.line.setAttribute("stroke", this.color);
    this.line.setAttribute("stroke-width", "5");
    this.g.appendChild(this.line);

    this.svgChart.nativeElement.appendChild(this.g);

    let translateDate;
    if (this.mode === 'week') {
      translateDate = new DatePipeProxy(this.translate);
    }
    else{
      translateDate = new TranslatePipe(this.translate, this.cdRef)
    }


    svgData.map(( coordinates, index )=>{
      if(this.showPoints){
        let point = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "circle"
        );
        point.setAttribute("cx",coordinates[0]);
        point.setAttribute("cy",coordinates[1]);
        point.setAttribute("r", ''+this.pointSize);
        point.setAttribute("fill", this.color);
        point.addEventListener('click', this.onClick.bind(this,coordinates[2]));
        //point.setAttribute("stroke", "#fff");
        //point.setAttribute("stroke-width", "2");
        this.g.appendChild(point);
      }});

      allSVGData.map(( coordinates, index )=>{
      let text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      text.setAttribute("x",coordinates[0]);
      text.setAttribute("y",height+50+ " ");
      text.setAttribute("text-anchor",'middle');
      text.setAttribute('font-family',"Lato-Black, Lato");
      text.setAttribute('font-weight',"800");
      text.setAttribute('fill', '#A8AAAC');
      if(this.mode==='week') {
        text.innerHTML = translateDate.transform(this.dayNameArray[index], 'EEEEE');
      }
      else if(this.mode==='month'){
        text.innerHTML = translateDate.transform('week_abbreviation') + (index+1);
      }
      this.g.appendChild(text)
    })
  }

  getValueArray( graphData ) {
    return graphData.map(( item ) => {
      return item.value;
    })
  }

  calculateSVGData( graphData, width, height ) {
    let values = this.getValueArray( graphData );
    return this.getCoordinates( values, width, height );
  }


  getCoordinates( values, width, height ) {
    //let min = Math.min.apply( null, values )-1;
    console.log(this.maxVar);
    let min = this.maxVar?0:Math.min.apply( null, values.filter(x=>x!==null) );//0;
    let max = this.maxVar?this.max:Math.max.apply( null, values );

    let diff = max-min;

    if(!this.maxVar) {
      min = min - diff * 0.3;
      max = max + diff * 0.3;
    }



    if(max==0){
      max=3;
    }

    let min2 =  0;//Math.min.apply( null, values.filter(x=>x!==null) );//(this.showPoints?0:0); //point size
    let max2 = height - (this.showPoints?0:0); //point size

    let yRatio = height / ( max - min );
    let xRatio = width / ( values.length );


    return values.map( function( value, i ) {

      let y = height - (yRatio * ( value - min )) ;
      console.log(height);
      console.log(value);
      console.log(min);
      let x = ( xRatio * i ) + ( xRatio / 2 );

      // rescale in order than point stay in view
      let y2 = min2 + (y/height) * (max2-min2);

      if(value==null){
        return [x,null, value];
      }
      return [x,y2, value]
    })
  }

  getLineSVG( data ) {
    console.log(data);
    let lineGenerator = d3.line()
      .x(function(d) { return d[0]; })
      .y(function(d) { return d[1]; })
      .curve(d3.curveCardinal);

    return lineGenerator(data)
  }

  getSimpleLineSVG( data ) {
    let lineGenerator = d3.line()
      .x(function(d) { return d[0]; })
      .y(function(d) { return d[1]; })
      .curve(d3.curveCardinal);

    return lineGenerator(data)
  }

  onClick(index, e){
    this.displayValue = index;
    console.log(e)
    console.log(this.data[index])
  }


}
