import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'nestore-number-picker',
  templateUrl: 'nestore-number-picker.component.html',
})
export class NestoreNumberPickerComponent {
  @Output() valid = new EventEmitter();
  number = 0;

  increment(e){
    this.number += e;
  }

  validate(){
    this.valid.emit(this.number);
  }
}
