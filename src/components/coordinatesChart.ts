import {ChangeDetectorRef, Input} from "@angular/core";
import {ChartOld} from "./chart";

export abstract class CoordinatesChartOld extends ChartOld {
  @Input() yLines = [] ;
  @Input() yLabels = [] ;
  @Input() mode ;
  @Input() max ;

  dayNameArray = [];
  height;
  g;
  g2;
  offsetY = 40;
  offset2 = 0;

  protected constructor(protected cdRef:ChangeDetectorRef) {
    super(cdRef);
    let baseDate = new Date(Date.UTC(2017,0, 1)); // just a Monday
    this.dayNameArray = [];
    for(let i = 0; i < 7; i++)
    {
      this.dayNameArray.push(baseDate);
      baseDate = new Date(baseDate.setDate(baseDate.getDate() + 1));
    }
  }

  public initChart() {
    if(this.g){
      this.g.remove();
    }
    if(this.g2){
      this.g2.remove();
    }
    if(this.mode){
      this.offsetY = 50
    }
    if(this.yLabels.length>0){
      this.offset2 = 50;
    }
    let width = this.svgChart.nativeElement.clientWidth;
    let height = width/4;
      this.svgChart.nativeElement.style.height=height+this.offsetY;
    this.height=height+this.offsetY;

    this.g= document.createElementNS(
      "http://www.w3.org/2000/svg",
      "g"
    );

    this.g.setAttribute("transform", "translate("+this.offset2+","+0+")");

    this.g2= document.createElementNS(
      "http://www.w3.org/2000/svg",
      "g"
    );

    this.g2.setAttribute("transform", "translate(0,0)");

    for(let label of this.yLabels){
      let value;
      if(label.value){
       value = label.value;
      }
      else{
        value = label;
      }

      let position = height-(height)*(value/this.max);
      console.log(position);

      let text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      text.setAttribute("x",""+30);
      text.setAttribute("y",position+5+"");
      text.setAttribute("text-anchor",'middle');
      text.setAttribute('font-family',"Lato-Black, Lato")
      text.setAttribute('font-weight',"800");
      text.setAttribute('fill', '#A8AAAC');
      text.innerHTML = ""+(label.label!=undefined?label.label:label);
      this.g2.prepend(text);
    }

    for(let line of this.yLines){
      let position = height-(height)*(line/this.max);
      let line1 = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "path"
      );
      line1.setAttribute("d", "M0," + (position) + " L" + width + ", " + (position));
      line1.setAttribute("fill", "none");
      line1.setAttribute("stroke", "#F0F0F0");
      line1.setAttribute("stroke-width", "2");
      console.log('testd');
      this.g2.prepend(line1);
    }

    this.svgChart.nativeElement.appendChild(this.g2);

  }


}
