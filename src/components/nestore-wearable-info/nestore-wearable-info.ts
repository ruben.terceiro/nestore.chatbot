import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from "@angular/core";
import {TranslatePipe, TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'nestore-wearable-info',
  templateUrl: 'nestore-wearable-info.html'
})
export class NestoreWearableInfoComponent {
  @Input() mode;

  @Output() close = new EventEmitter();
  selectPage = 0;
  pages = 3;
  page = 0;
  articles;
  translatePipe;

  quit(){
    this.close.next();
  }

  constructor(private cdRef:ChangeDetectorRef, private translate: TranslateService){
    this.translatePipe = new TranslatePipe(this.translate,this.cdRef);

    this.articles = [];
  }

  ngOnInit(){
    if(this.mode!=='test') {
      this.articles.push({
        img: 'assets/icon/blue_wearable.svg',
        title: this.translatePipe.transform('wearable-1_title'),
        text: this.translatePipe.transform('wearable-1_info')
      });
      this.articles.push({
        img: 'assets/icon/yellow_wearable.svg',
        title: this.translatePipe.transform('wearable-2_title'),
        text: this.translatePipe.transform('wearable-2_info')
      });
    }
    this.articles.push({
      img: 'assets/icon/watch_button.svg',
      title: this.translatePipe.transform('start_activity'),
      text: this.translatePipe.transform('start_structured_activity_instruction')
    });
    this.pages = this.articles.length;
  }
}
