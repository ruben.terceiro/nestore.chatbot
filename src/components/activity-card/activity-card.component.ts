import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import { GlobalvarProvider } from '../../app/globalprovider';
import {LoginPage} from '../../pages/login/login';
import {IamClientProvider} from '../../providers/iam-client/iam-client';
import {App} from 'ionic-angular';
import {TranslatePipe, TranslateService} from "@ngx-translate/core";
import {TutorialService} from "../../providers/tutorialService";
import {Subject} from "rxjs";

@Component({
  selector: 'nestore-activity-card',
  templateUrl: 'activity-card.component.html',
})
export class ActivityCardComponent {
  @Input() image;
  @Input() domain = 'physical';
  @Input() data;
  @Input() toPlan;

  @Output() dataChange = new EventEmitter();

  @Input() hasValidation = true;
  @Output() validate = new EventEmitter();
  @Output() schedule = new EventEmitter();
  @Output() start = new EventEmitter();
  @Output() showDetails = new EventEmitter();

  plan = new Subject();

  structuredText = [];

  timeToWait;

  translatePipe = new TranslatePipe(this.translate, this.cdRef);

    constructor(private cdRef:ChangeDetectorRef, private translate: TranslateService, private globalprovider : GlobalvarProvider, private tutorialService: TutorialService) {
    this.translatePipe = new TranslatePipe(this.translate, this.cdRef);
    this.plan.subscribe(()=>{
      this.valid();
    })

  }

  ngOnInit(){

    if(this.data['scheduled-time']) {
      setInterval(() => {
        this.timeToWait = (new Date(this.data['scheduled-time']).getTime() - new Date().getTime()) / 1000;
      }, 1000);
    }
  }


  ngOnChanges(changes){
    console.log(this.data);

    if(changes.data){
      if(this.data.ce_id) {
        let text = this.translatePipe.transform(this.data.ce_id);
        let links = text.match(/\bhttps?:\/\/\S+/gi);

        let endMessage = text;
        if (links) {
          for (let link of links) {
            let split = endMessage.split(link);
            this.structuredText.push({type: 'text', data: split[0]});
            this.structuredText.push({type: 'link', data: link});
            endMessage = split[1];
            console.log(this.structuredText);

          }
        }
      }
    }
  }

  valid(){
    if(this.data.review && this.data.review.done){
      //nothing
    }
    else if(this.data['scheduled-time'] && this.data.domain==='physical' && this.data['workout-id']){
      this.startActivity(this.data)
    }
    else if(this.data.scheduled==true && this.data.title==='ce_cog_nut' && this.data.task){
      this.startActivity({game:'NUT', task:this.data.task.id})
    }
    else if(this.data['scheduled-time']){
      this.validate.emit();
      this.data.loading = true;
    }
    else if(this.data && !this.data.planned && !(this.data.review && this.data.review.done)){
      this.schedule.emit();
    }
    else if(this.data.task) {
      this.validate.emit(this.data.task);
    }
    else {
      this.validate.emit();
    }
  }

  scheduleActivity(){
   this.schedule.emit();
  }

  startActivity(activity){
    this.start.emit(activity);
  }

  details(){
    console.log(this.data);
    this.showDetails.emit();
  }

  openLink(link){
    console.log('link');
    window.open(link,'_system', 'location=yes');
  }

  playOdyssey(){
    if((<any>window).plugins){ // useful when we test without deploying
      (<any>window).plugins.intentShim.startActivity({
          //https://github.com/darryncampbell/plugin-intent-api-exerciser/blob/master/www/js/index.js#L120
          component:
            {
              "package": "com.tud.NestorePocketOdyssey",
              "class": "com.unity3d.player.UnityPlayerActivity"
            }
        },
        function() {console.log('eerr')},
        function() {
          if((<any>window).plugins){ // useful when we test without deploying
            (<any>window).plugins.intentShim.startActivity({
                //https://github.com/darryncampbell/plugin-intent-api-exerciser/blob/master/www/js/index.js#L120
                action: (<any>window).plugins.intentShim.ACTION_VIEW,
                url:"market://details?id=com.tud.NestorePocketOdyssey"
              },
              function() {},
              function() {}
            );
          }

        }
      );
    }
    }



}
