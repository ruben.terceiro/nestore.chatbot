import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'numerical-keyboard',
  templateUrl: 'numerical-keyboard.html',
})
export class NumericalKeyboardComponent {
  @Input() validation = false;
  @Input() size = 1;
  @Output() output = new EventEmitter();

  number = "";
  text = "";

  ngOnInit(){
    this.generateText();
  }

  select(num){
    if(this.validation){
      if(this.number.length<this.size) {
        this.number += num;
        this.generateText();
      }
    }
    else {
      this.validate(num)
    }
  }

  clear(){
    this.number = this.number.substring(0,this.number.length-1);
    this.generateText();
  }

  validate(num){
    console.log(num);
    console.log(this.number);
    this.output.emit(num!==undefined?num:this.number)
  }

  generateText(){
    this.text = "";
    for(let i = 0; i<this.size-this.number.length; i++){
      this.text += "_";
    }
  }
}
