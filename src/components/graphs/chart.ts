import {AfterViewInit, ChangeDetectorRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';

export abstract class Chart implements AfterViewInit {
  @ViewChild('svg') svgChart;
  @Input() protected data;
  @Input() color: any = '#76C1DB';

  type;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.initChart();
  }

  protected constructor(protected cdRef: ChangeDetectorRef, type) {
    this.cdRef = cdRef;
    this.type = type;
  }

  public abstract initChart();

  ngAfterViewInit() {
    if (typeof this.color === 'string') {
      let temp = this.color;
      this.color = [];
      this.color.push(temp);
    }
    this.initChart();
    this.cdRef.detectChanges();
  }

}
