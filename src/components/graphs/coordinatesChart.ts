import {ChangeDetectorRef, Input} from '@angular/core';
import {Chart} from './chart';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {DatePipeProxy} from "../../pipes/DatePipeProxy";
export abstract class CoordinatesChart extends Chart {
  @Input() yLines = []; // array of values for displayed horizontal lines
  @Input() yLabels = [] ; // array of values for label value shown
  @Input() thresolds = [];
  @Input() mode; // 'week' | 'month'
  @Input() max;
  @Input() min;
  @Input() yAxis;
  @Input() showValues;
  @Input() showSubValues;
  @Input() xLabels = [] ; // array of values for label value shown


  svgData;

  dayNameArray = [];
  height;
  g;
  g2;
  offsetY = 40;
  offset2 = 0;

  graphHeight;
  graphWidth;

  protected constructor(protected cdRef: ChangeDetectorRef, protected translate: TranslateService, type) {
    super(cdRef, type);
    let baseDate = new Date(Date.UTC(2017, 0, 1)); // just a Monday
    this.dayNameArray = [];
    for (let i = 0; i < 7; i++) {
      this.dayNameArray.push(baseDate);
      baseDate = new Date(baseDate.setDate(baseDate.getDate() + 1));
    }
  }

  public abstract calculateSVGData(a, width, height);

  public initChart() {
    const set = [];
    if (Array.isArray(this.data[0])) {
      for(let entry of this.data){
        set.push(entry);
      }
    } else{
      set.push(this.data);
    }

    if (this.g) {
      this.g.remove();
    }
    if (this.g2) {
      this.g2.remove();
    }
    if (this.mode) {
      this.offsetY = (this.type === 'bar' ? 20 : 50) + (this.showValues ? 20 : 0)  + (this.showSubValues ? 20 : 0);
    }
    if (this.yLabels.length > 0 || this.thresolds.length > 0 || this.yLines.find((x) => x.label)) {
      this.offset2 = 50;
    }


    if (this.yAxis === 'auto') {
      const minValue = Math.min.apply( null, set[0].filter(x => x !== null ) ); // 0;
      let maxValue = this.getMaxOfArrays(set);
      console.log(maxValue);
      if (this.yLines) {
        const yLinesMaxValue = Math.max.apply( null, this.yLines.map((e) =>
          typeof e === 'number' ? e : e.value
        )); // 0;
        const yLabelsMaxValue = Math.max.apply( null, this.yLabels); // 0;
        const yThresholdMaxValue = Math.max.apply( null, this.thresolds.map((e) =>
          typeof e === 'number' ? e : e.value
        )); // 0;
        maxValue = Math.max(maxValue, yLabelsMaxValue, yThresholdMaxValue, yLinesMaxValue);
      }
      let diff = maxValue - minValue;
      if (diff === 0) {
        maxValue = maxValue + 1;
      }
      diff = maxValue - minValue;
      this.max = maxValue + (this.type === 'bar' ? 0.2 : 0.5)  * diff;
      if (!this.min && this.min !== 0) {
        this.min = minValue - 0.5 * diff;
      }
    } else {
      let max = Math.max.apply( null, set[0]);
      for (const entry of set) {
        max = Math.max(max, Math.max.apply( null, entry));
      }
      this.max = this.max ? this.max : 1.3 * max;
      this.min = 0;
    }
    console.log(this.min);

    this.graphWidth = this.svgChart.nativeElement.clientWidth;
    this.graphHeight = this.graphWidth / (this.type === 'bar' ? 3 : 4);
    this.height = this.graphHeight + this.offsetY;
    this.svgChart.nativeElement.style.height = this.height;

    let translateDate;
    const translatePipe = new TranslatePipe(this.translate, this.cdRef);
    if (this.mode === 'week') {
      translateDate = new DatePipeProxy(this.translate);
    } else {
      translateDate = new TranslatePipe(this.translate, this.cdRef);
    }


    this.g = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'g'
    );

    this.g.setAttribute('transform', 'translate(' + this.offset2 + ',' + 0 + ')');

    this.g2 = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'g'
    );

    this.g2.setAttribute('transform', 'translate(0,0)');

    for (const label of this.yLabels) {
      let value;
      if (label.value) {
        value = label.value;
      } else {
        value = label;
      }

      const position = this.graphHeight - (this.graphHeight) * ((value - this.min) / (this.max - this.min));
      console.log(position);

      const text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      text.setAttribute("x",""+30);
      text.setAttribute("y",position+5+"");
      text.setAttribute("text-anchor",'middle');
      text.setAttribute('font-family',"Lato-Black, Lato")
      text.setAttribute('font-weight',"800");
      text.setAttribute('fill', '#A8AAAC');
      text.innerHTML = '' + (label.label !== undefined ? label.label : label);
      this.g2.prepend(text);
    }

    for (let line of this.thresolds) {
      if(typeof line === 'number'){
        line = {value: line};
      }

      let position = this.graphHeight - (this.graphHeight) * ((line.value - this.min) / (this.max - this.min));

      const text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      text.setAttribute("x",""+27);
      text.setAttribute("y",position+5.5+"");
      text.setAttribute("text-anchor",'middle');
      text.setAttribute('font-family',"Lato-Black, Lato")
      text.setAttribute('font-weight',"800");
      text.setAttribute('fill', 'white');
      text.innerHTML = '' + (line.label ? line.label : line.value);
      this.g2.prepend(text);


      let label = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "path"
      );
      label.setAttribute("d", "M1528.559,720.7h-24.93a3.707,3.707,0,0,1-3.707-3.707V703.435a3.707,3.707,0,0,1,3.707-3.707h24.93a3.708,3.708,0,0,1,3.056,1.608l4.655,6.779a3.707,3.707,0,0,1,0,4.2l-4.655,6.779A3.707,3.707,0,0,1,1528.559,720.7Z");
      label.setAttribute("fill", "#333");
      label.setAttribute("transform", "translate(-1490 " + (-710 + position) +")");
      this.g2.prepend(label);

      let line1 = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "path"
      );
      line1.setAttribute("d", "M30," + (position) + " L" + (this.graphWidth-30) + ", " + (position));
      line1.setAttribute("fill", "none");
      line1.setAttribute("stroke", "black");
      line1.setAttribute("stroke-width", "2");
      this.g2.prepend(line1);
    }


    for (let line of this.yLines) {
      if(typeof line === 'number'){
        line = {value: line};
      }

      let position = this.graphHeight - (this.graphHeight) * ((line.value - this.min) / (this.max - this.min));

      let text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      if(line.label) {
        text.setAttribute("x", "" + 30);
        text.setAttribute("y", position + 5 + "");
        text.setAttribute("text-anchor", 'middle');
        text.setAttribute('font-family', "Lato-Black, Lato")
        text.setAttribute('font-weight', "800");
        text.setAttribute('fill', '#A8AAAC');
        text.setAttribute('font-size', "15px");
        text.innerHTML = "" + line.label;
        this.g2.prepend(text);
      }

      console.log(line)
      let line1 = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "path"
      );
      line1.setAttribute("d", "M0," + (position) + " L" + this.graphWidth + ", " + (position));
      line1.setAttribute("fill", "none");
      line1.setAttribute("stroke", "#F0F0F0");
      line1.setAttribute("stroke-width", "2");
      this.g2.prepend(line1);



    }

    this.svgChart.nativeElement.appendChild(this.g2);

    this.svgData = []
    for(let entry of set) {
      this.svgData.push(this.calculateSVGData(entry.map((v) => {
        return {value: v};
      }), this.graphWidth - this.offset2, this.graphHeight));
    }

    this.svgData[0].map(( coordinates, index )=>{
      let text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      text.setAttribute("x",coordinates[0]);
      text.setAttribute("y",this.graphHeight+ (this.type === 'bar' ? 20 : 50) + (this.showValues?20:0) + (this.showSubValues?20:0)+" ");
      text.setAttribute("text-anchor",'middle');
      text.setAttribute('font-family',"Lato-Black, Lato");
      text.setAttribute('font-weight',"800");
      text.setAttribute('font-size', "15px");
      text.setAttribute('fill', '#A8AAAC');
      if (this.mode === 'week') {
        text.innerHTML = translateDate.transform(this.dayNameArray[index], 'EEEEE');
      } else if (this.mode === 'month') {
        text.innerHTML =  translateDate.transform('week_abbreviation') + (index+1);
      }
      this.g.appendChild(text);

      if (this.showValues) {
        text = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "text"
        );
        text.setAttribute("x", coordinates[0]);
        text.setAttribute("y", this.graphHeight + 20 + " ");
        text.setAttribute("text-anchor", 'middle');
        text.setAttribute('font-family', "Lato-Black, Lato");
        text.setAttribute('font-weight', "800");
        text.setAttribute('fill', '#333333');
        text.setAttribute('font-size', "3.5vw");

        text.innerHTML = coordinates[3]!==null ? coordinates[3] +'%' : '';
        this.g.appendChild(text);

        if(this.showSubValues) {
          text = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "text"
          );
          text.setAttribute("x", coordinates[0]);
          text.setAttribute("y", this.graphHeight + 35 + " ");
          text.setAttribute("text-anchor", 'middle');
          text.setAttribute('font-family', "Lato");
          text.setAttribute('font-weight', "100");
          text.setAttribute('font-size', "3.5vw");
          text.setAttribute('fill', '#333333');
          text.innerHTML = this.xLabels && this.xLabels.length > 0 && this.xLabels[index % this.xLabels.length] !== null ? '' + this.xLabels[index % this.xLabels.length] : '-';
          this.g.appendChild(text);
        }
      }

    });

    if (this.showSubValues) {
      let text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      text.setAttribute("x", -40 + '');
      text.setAttribute("y", this.graphHeight + 35 + " ");
      text.setAttribute("text-anchor", 'left');
      text.setAttribute('font-family', "Lato");
      text.setAttribute('font-weight', "100");
      text.setAttribute('fill', '#333333');
      text.setAttribute('font-size', "3vw");
      text.innerHTML = translatePipe.transform('level');
      this.g.appendChild(text);
    }

  }

  getMaxOfArrays(set) {
    let bigArray = [];
    for (let s of set) {
      bigArray = bigArray.concat(s);
    }
    return Math.max.apply( null, bigArray.filter(x => x !== null ) ); // 0;


  }


}
