import {ChangeDetectorRef, Component, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CoordinatesChart} from '../coordinatesChart';


@Component({
  selector: 'nestore-bar-chart',
  templateUrl: 'nestore-bar-chart.html'
})
export class NestoreBarChartComponent extends CoordinatesChart {
  @Input() label;
  @Input() rounded = false;
  @Input() background;
  @Input() offset;

  backgroundHeight=0;

  line1;
  line2;
  line3;
  line;

  constructor(protected cdRef: ChangeDetectorRef, protected translate: TranslateService) {
    super(cdRef, translate, 'bar');
  }

  initChart() {
    if (this.g) {
      this.g.remove();
    }
    if (this.line) {
      this.line.remove();
    }
    if (this.line2) {
      this.line2.remove();
    }
    if (this.line3) {
      this.line3.remove();
    }
    if (this.line1) {
      this.line1.remove();
    }

    super.initChart();

    //const width = this.svgChart.nativeElement.clientWidth;
    //const height = width / 2;

    /*
    const svgData = this.calculateSVGData( this.data.map((v) => {
      if (this.rounded && v !== null) {
        return {value: v[0], offset: v[1]};
      }
      return {value: v};
    }), width - 50, height );*/


    this.line3 = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'path'
    );

    this.svgChart.nativeElement.appendChild(this.g);

    let size = this.svgData.length;
    for (let [i,entry] of this.svgData.entries()) {
      entry.map((coordinates, index) => {
        const g2 = document.createElementNS("http://www.w3.org/2000/svg", "g");

        /*let w = width / 20;
        if (this.rounded) {
          g2.setAttribute('clip-path', 'inset(0 0 0 0 round 80)');
          g2.setAttribute('transform', 'translate(0,'+coordinates[2]+')');
          w = width / 30;

        }*/
        const w = this.graphWidth / (size > 1 ? 15 : 20);
        let height = this.graphHeight;

        console.log(w);
        const offset =  i * w * (1 / size);


        console.log(this.background);
        if(this.background && coordinates[3]) {
          let point = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "rect"
          );
          point.setAttribute("x", "" + (-w / 2 + coordinates[0] + offset));
          point.setAttribute("y", "" + ((height) - (height - this.backgroundHeight)));
          point.setAttribute("width", "" + w * (1 / size));
          point.setAttribute("height", "" + (height - this.backgroundHeight));
          point.setAttribute("fill", '#EFEFEF');
          g2.appendChild(point);
        }

        let point = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "rect"
        );
        point.setAttribute("x", "" + (-w / 2 + coordinates[0] +  offset));
        point.setAttribute("y", "" + ((height) - (height - coordinates[1])));
        point.setAttribute("width", "" + w * (1/size));
        point.setAttribute("height", "" + (height - coordinates[1]));
        point.setAttribute("fill", this.background?this.colorFromValue(coordinates[3]):this.color[i % this.color.length]);



        if (this.rounded) {
          g2.setAttribute('clip-path', 'inset(0 0 0 0 round 80)');
        }
        if (this.offset) {
          const position = (this.graphHeight) * ((this.offset[index] - this.min) / (this.max - this.min));
          g2.setAttribute('transform', 'translate(0,' + - position + ' ) ');
        }
        //point.setAttribute("stroke", "#fff");
        //point.setAttribute("stroke-width", "2");
        g2.appendChild(point);
        this.g.appendChild(g2);
      });
    }
  }

  calculateSVGData( graphData, width, height ) {

    const min = 0;
    const max = this.max;
    let yRatio = height / ( max - min );
    let y = height - (yRatio * ( 100 - min ));
    this.backgroundHeight = y;

    return this.getCoordinates( graphData, width, height);
  }

  getCoordinates( graphData, width, height ) {
    let v =  graphData.map(( item ) => {
      return item.value;
    });

    let min = 0;
    let max = this.max ? this.max : Math.max.apply( null, v );

    let min2 = 0;
    let max2 = height - 0;

    let yRatio = height / ( max - min );
    let xRatio = width / ( v.length );

    return graphData.map( ( value, i ) => {
      let v = value.value;
      let off = 0;
      if(this.rounded && value.value!==null){
        v = value.value;
        off=value.offset;
      }
      let y = height - (yRatio * ( v - min )) ;
      let x = ( xRatio * i ) + ( xRatio / 2 );

      // rescale in order than point stay in view
      let y2 = min2 + (y/height) * (max2-min2);

      let offset = - (yRatio * ( off - min )) ;
      let offset2 = min2 + (offset/height) * (max2-min2);

      return [x, y2 , off?offset2:0, v];
    });
  }

  colorFromValue(v) {
    if (v < 50) {
      return '#FD6E5B';
    } else if (v < 75) {
      return '#FFD938';
    }
    return '#96E24B';
  }


}
