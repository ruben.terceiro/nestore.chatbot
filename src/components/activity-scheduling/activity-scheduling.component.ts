import {Component, EventEmitter, Input, Output} from '@angular/core';
import {APIService} from "../../chat/services/api";
import {TranslateService} from "@ngx-translate/core";
import {GlobalvarProvider} from "../../app/globalprovider";
import {TutorialService} from "../../providers/tutorialService";
import {Subject} from "rxjs";


@Component({
  selector: 'activity-scheduling',
  templateUrl: 'activity-scheduling.component.html',
})


export class ActivitySchedulingComponent {
  @Input() activity;
  @Input() mode;
  @Output() cancel = new EventEmitter();
  @Output() scheduled = new EventEmitter();

  loaded = true;
  loadedHours = true;
  isCalendarEnabled = false;
  date = new Date();
  availableTime;
  availableHours;
  unavailableTime;
  selectedDateTime = null;
  dateArray;
  selected;
  nbSelected = 0;
  nbHourSelected = 0;
  numberOfSessions = 0;

  save = new Subject();

  constructor(public api: APIService, public globalProvider : GlobalvarProvider, public tutorialService : TutorialService) {
    let number = this.globalProvider.profile.dss_profile.working_memory.planDay>5? 0:this.globalProvider.profile.dss_profile.working_memory.planDay+1;

    let date = new Date();

    let number2 = date.getDay();
    console.log(number);
    console.log(number2);
    let diff = Math.abs(number2-number);
    if(diff == 0){
      diff = 7;
    }

    date.setDate(date.getDate() - 1);
    this.dateArray = [];
    for(let i = 0; i < diff; i++)
    {
      this.dateArray.push({date:date, selected: false});
      date = new Date(date.setDate(date.getDate() + 1));
    }

    this.save.subscribe(fromTuto=>{
      if(!fromTuto){
        this.clickOK()
      }
    })

  }

  ngOnChanges(change) {
    if (change.activity.currentValue) {
      console.log(change.activity.currentValue);
      this.numberOfSessions = Math.min(this.dateArray.length, this.activity.activities.length);
      if(this.numberOfSessions == this.dateArray.length) {
        this.dateArray.forEach(x => {
          this.select(x);
          return x
        });
      }
      this.changeDate()
    }
  }

  clickOK() {
    this.loaded = false;
    let promises = [];

    for(let i = 0; i < this.nbSelected; i++) {
      promises.push(this.api.postActivitySchedule(this.activity.activities[i].id, this.dateArray.filter(x=>x.selected)[i].selectedDateTime, this.activity.activities[i].eventId))
    }

    Promise.all(promises).then(
      ()=>{
        this.loaded=true;
        this.scheduled.next()}).catch(()=>{
          this.globalProvider.internalError = true;
      this.loaded=true;
      this.scheduled.next()
        })
  }

  clickCancel() {
    this.cancel.next();
  }

  isToday(td) {
    let d = new Date();
    return td.getDate() === d.getDate() && td.getMonth() === d.getMonth() && td.getFullYear() === d.getFullYear();
  }

  changeDate(){
    if(!this.unavailableTime) {
      this.loadedHours = false;
      this.api.getUnavailableTimeSlots(this.activity.activities[0].id).then((res) => {
          let times = [];
          for (let a of res) {
            times.push(+new Date(a));
          }
          this.unavailableTime = times;
          this.s();
          this.loadedHours = true;
        }
      );
    }
    else{
      this.s()
    }
  }

  s(){
    this.dateArray.forEach(x => {
      x.availableHours = [];
      for (let i of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]) {
        let d = new Date(x.date);
        d.setHours(i);
        d.setMinutes(0);
        d.setSeconds(0);
        d.setMilliseconds(0);
        console.log(this.unavailableTime);
        console.log(+new Date(d));
        console.log(!this.unavailableTime.includes(+new Date(d)) && +new Date() < +new Date(d));
        if(!this.unavailableTime.includes(+new Date(d)) && +new Date() < +new Date(d)) {
          x.availableHours.push(d);
        }
      }
    });

    console.log(this.dateArray);

  }

  select(d){
    if(this.numberOfSessions===1){
      this.dateArray.forEach((d)=>{
        d.selected = false;
        d.selectedDateTime=null;
      })
    }
    d.selected = !d.selected;
    this.nbSelected = this.dateArray.filter(x=>x.selected).length;
    this.nbHourSelected = this.dateArray.filter(x=>x.selectedDateTime).length;
  }

  selectHour(d, a){
    d.selectedDateTime=a;
    this.nbHourSelected = this.dateArray.filter(x=>x.selectedDateTime).length;
  }

  quit(){
    this.cancel.emit();
  }
}
