import {Component, Input, SimpleChanges} from '@angular/core';
import {APIService} from '../../../chat/services/api';

/**
 * Generated class for the LonelinessMonthComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nestore-loneliness-month',
  templateUrl: 'loneliness-month.html'
})
export class LonelinessMonthComponent {
  showdetails = false;
  dayInitial = ['M','T','W','T','F','S','S']; //TODO: Use a library to get it in the four languages
  @Input() date = new Date();

  @Input() loneliness;

  @Input() colorThresholds: object[] = [
    {'from': 0, 'to': 0, 'color': '#FD6E5B'}, // not lonely
    {'from': 1, 'to': 1, 'color': '#FFD938'}, // somewhat lonely
    {'from': 2, 'to': 2, 'color': '#96E24B'}, // lonely
  ];

  dataset: {dayOfWeek:number, value:number, color: string}[][];

  constructor(private api: APIService){
  }

  ngOnChanges(changes: SimpleChanges) {
    //if(changes.date){
    // this.getData();
    //}
    if(changes.loneliness){
      this.computeChart();
    }
  }

  getData(){
    return this.api.getLoneliness(this.date.toISOString().substring(0,10),'month').then(data =>
      {
        this.loneliness = data;
        this.computeChart();
      }
    )
  }

  @Input() defaultColor: string = '#F0F0F0';

  totals = [0,0,0];
  nbFull = 0;


  computeChart() {
    this.nbFull = 0;
    this.totals = [0,0,0];
    this.dataset = [];
    console.log(this.loneliness);
    for(let i = 0; i < this.loneliness.length; i++) {
      let weekDataset = [];
      for(let j = 0; j < this.dayInitial.length; j++) {
        let elt = null;
        for(let k = 0; k < this.loneliness[i].length; k++) {
          if(j == this.loneliness[i][k].dayOfWeek) {
            elt = this.loneliness[i][k];
            break;
          }
        }
        if(elt != null) {
          let color: string = null;
          for(let k = 0; k < this.colorThresholds.length; k++) {
            if(elt.value===null){
              color = this.defaultColor;
            }
            if(elt.value >= this.colorThresholds[k]['from'] && elt.value <= this.colorThresholds[k]['to']) {
              color = this.colorThresholds[k]['color'];
              this.totals[elt.value]+=1;
            }
          }
          weekDataset.push({dayOfWeek: elt.dayOfWeek,
            value: this.dayInitial[elt.dayOfWeek], color: color
          });
        } else {
          weekDataset.push({dayOfWeek: j,
            value: null, color: this.defaultColor
          });
        }
      }
      console.log('week', weekDataset);
      this.dataset.push(weekDataset);
      console.log('final', this.dataset);
    }
    for(let i of this.totals){
      this.nbFull += i;
    }
  }

}
