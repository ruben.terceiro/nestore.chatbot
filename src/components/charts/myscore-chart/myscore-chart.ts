import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-myscore-chart',
  templateUrl: 'myscore-chart.html'
})
export class MyscoreChartComponent {
  @Input() timeframe;
  @Input() data;
  @Input() total;

  value;
  ngOnChanges(change){
    if(change.timeframe){
      if(this.timeframe==='week'){
        this.total = 30;
      }
      else if(this.timeframe==='month'){
        this.total = 120;
      }
    }
    if(change.data) {
      if(!this.data.score_non_structured)
      {
        this.data.score_non_structured=0;
      }
      if(!this.data.score_structured)
      {
        this.data.score_structured=0;
      }
      this.value = (this.data.score_non_structured+this.data.score_structured).toFixed(1);

    }
  }
}
