import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-structured-activity-chart',
  templateUrl: 'structured-activity-chart.html'
})
export class StructuredActivityChartComponent {
  @Input() timeframe;
  @Input() data;
  chartsData;
  showdetails = false;


  chartColors: string[] = [
    '#B14854',
    '#FDB2A4',
    '#FF6375'
  ];

  chartDataSets: ChartDataSets[];
  chartLegend: boolean = false;
  chartLabels: string[][];
  chartType: ChartType = 'doughnut';
  chartOptions: ChartOptions = {
    animation: {duration:0},
    responsive: true,
    segmentShowStroke: false,
    cutoutPercentage: 70,
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        maintainAspectRatio: true,
      }],
      yAxes: [{
        ticks: {
          display: false,
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
      }],
    }
  };

  ngOnChanges(changes){
    if(changes.data){
      if(this.timeframe==='month' || this.timeframe==='week') {
        this.chartsData = this.data.dayValues.map(x => {
          return x.map(y=>{return {value: y.value, color: y.value && y.value.total?'#FF6375':'#F0F0F0'}});
        })
      }
      if(this.data && !this.data.training_adherence){
        this.data.training_adherence = 0;
      }
    }
  }

  constructor() {
    let data = [40,40,20];
    this.chartDataSets = [
      {data: data, label: 'data1', fill: false, backgroundColor: this.chartColors},
    ];
  }

  convertHour(seconds: number): string {
    let hours =  Math.floor((seconds / 3600));
    return Math.floor(seconds / 3600) + 'h' + (seconds !== 0 ?('0'+Math.floor((seconds - hours*3600)/60)).slice(-2):'');
  }
}
