import {Component, Input} from '@angular/core';

@Component({
  selector: 'nestore-omega3-chart',
  templateUrl: 'omega3-chart.html'
})
export class Omega3ChartComponent {
  @Input() timeframe;
  @Input() value : any = 0;
  @Input() total = 500;

  ngOnChanges(changes){
    if(changes.value){

      if(typeof this.value === 'number') {
        this.value = Math.round((this.value * 1000) /*grams*/ * 100) / 100;
      }
      else if(this.value instanceof Array) {
        console.log('array');
        this.value = this.value.map(x=>Math.round((x * 1000) /*grams*/ * 100) / 100);
      }
    }
  }
}


