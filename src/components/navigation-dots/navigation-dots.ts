import {
  Component,
  ContentChildren, ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  SimpleChanges, TemplateRef,
  ViewChild
} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';

/**
 * Generated class for the AllComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'navigation-dots',
  templateUrl: 'navigation-dots.html'
})

export class NavigationDotsComponent {
  @Input() page;
  @Input() pages;
  @Input() color = 'white';

  pageArray;

  ngOnChanges(change){
    if(change.pages){
      this.pageArray = Array.from({length:this.pages}, (v,k)=>k);
    }
  }
}
