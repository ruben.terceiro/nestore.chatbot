  import {Component} from "@angular/core";
import {GlobalvarProvider} from "../../app/globalprovider";

@Component({
  selector: 'nestore-pathway-info',
  templateUrl: 'nestore-pathway-info.html'
})
export class NestorePathwayInfoComponent {
  constructor(public globalvarProvider: GlobalvarProvider){

  }

}
