import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import { GlobalvarProvider } from '../../app/globalprovider';
import {LoginPage} from '../../pages/login/login';
import {IamClientProvider} from '../../providers/iam-client/iam-client';
import {App} from 'ionic-angular';
import {Message} from "../../chat/message/message.model";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {Subject} from "rxjs";
import {MessageProvider} from "../../chat/services/message-provider";
import {APIService} from "../../chat/services/api";
import {TutorialService} from "../../providers/tutorialService";

@Component({
  selector: 'nestore-header',
  templateUrl: 'nestore-header.html',
})
export class NestoreHeaderComponent {
  @Input() params;
  @Input() color;
  @Input() textColor = 'black';
  @Input() title;
  @Input() type;
  @Input() back;
  @Input() bigFont;
  @Input() chat =false;
  @Input() shadow = true;
  @Input() uppercase = true;
  @Input() options;
  @Input() notificationNum;
  @Input() tutoActivated;
  @Output() clickQuestionnaire = new EventEmitter();
  @Output() onSettings = new EventEmitter();
  @Output() onBack = new EventEmitter();
  messages;
  lastMessage : Message;

  clickBack = new Subject();

  openOptions = false;

  constructor(public api: APIService, public localNotifications: LocalNotifications,private messageProvider: MessageProvider,private app: App, public iamClient: IamClientProvider,public globalvarProvider: GlobalvarProvider, public tutorialService: TutorialService, private _ref : ChangeDetectorRef) {
    this.messageProvider.messagesChange
      .subscribe(
        (message : Message) => {
          if(!message.mode || !message.mode.startsWith('questionnaire')) {
            this.lastMessage = message;
          }
        });

    this.clickBack.subscribe((fromTuto)=>{
      this.onBack.emit();
    })
  }

  openChat(){
    if(this.lastMessage) {
      this.lastMessage.isRead = true;
      this.messageProvider.readAll();
    }
    this.globalvarProvider.openChat();
    this.localNotifications.cancelAll();
  }

  setting(){

    this.onSettings.emit();
  }

  toogleOpenOption(){
    //this.globalvarProvider.addTutorial('chatBack');

    this.openOptions = !this.openOptions;
  }

  logout(){
    this.api.deleteFCMToken().then(()=>{
      this.iamClient.logout().subscribe(next => {
        this.globalvarProvider.logoutEvent.next();


      }, error => {
        this.globalvarProvider.logoutEvent.next();
        //this.app.getRootNav().setRoot(LoginPage);
        console.log(error);

      },);
    }) // Avoid to send notification if user is logged out. For not blocking, we don't wait the result : If not succeed FCMtoken is not delete on server.

  }

  clickOutside(){

    this.openOptions=false;
  }

  fillInQuestionnaire() {
    this.messageProvider.fillInQuestionnaire(this._ref);
  }

}
