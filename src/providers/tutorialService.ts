import {Injectable} from '@angular/core';
import {GlobalvarProvider} from "../app/globalprovider";
import {MessageProvider} from "../chat/services/message-provider";
import {Storage} from "@ionic/storage";

@Injectable()
export class TutorialService {
  constructor(private globalProvider : GlobalvarProvider, private MessageProvider: MessageProvider, private storage: Storage) {

  }

  public showTuto = 0;
  public showTip = false;
  public tutoTitle;
  public tutorials: any = {};
  public tutorialDescription;
  public tutorialTitle;
  public tutorialCurrent;
  public tutorialSubject;

  public tutorialC;

  nextTip() {
    this.tutorials[this.tutorialCurrent] = {'activated': false};
    let currentList = this.list.find(x=>x.name===this.tutorialC).flow;
    let el = currentList.findIndex(x => x.name === this.tutorialCurrent);
    if(currentList[el].actionAfter){
      currentList[el].actionAfter();
    }
    if(this.tutorialSubject) {
      this.tutorialSubject.next(true);
    }
    if (el >= 0 && el + 1 < currentList.length) {
      this.addTutorial(this.tutorialC,currentList[el + 1].name);
    }
    else{
      this.showTuto=0;
      this.finish();
    }
  }

  copyElement(e, left, position, subject, name){
    console.log(position);
    let c = document.getElementById('canvas');
    while (c.firstChild) {
      c.removeChild(c.firstChild);
    }
    console.log(e.style);
    let cln = <HTMLElement>e.cloneNode(true);
    this.tutorialCurrent = name;
    this.tutorialSubject = subject;

    cln.addEventListener('click', ()=>{
      this.nextTip();
    });
    let divOffset = this.offset(e);
    console.log(divOffset);
    cln.style.position = 'absolute';
    cln.style.top = divOffset.top+"px";
    if(left) {
      cln.style.left = divOffset.left + "px";
    }
    console.log(e.clientWidth);
    console.log(e.getBoundingClientRect());
    cln.style.width =  e.clientWidth+'px';
    cln.style.height =  e.clientHeight+'px';

    //workaround to avoid to open file chooser in tutoNutrition
    if(cln.children[2] && cln.children[2]['htmlFor']){
      cln.children[2]['htmlFor'] = '';
    }


    c.appendChild(cln);

    let c2 = document.getElementById('tutorialWindowText');
    console.log(c.offsetHeight);
    console.log(divOffset.top);
    if(position === 'bottom') {
      console.log('bottom');
      c2.style.bottom = null;
      c2.style.top = divOffset.top + e.clientHeight + 'px';
    }
    else if(position==='top'){
      console.log('top');
      c2.style.top = null;
      c2.style.bottom = c.clientHeight - divOffset.top + 'px';
    }
    else if(position==='under') {
      let values3 = c.getElementsByClassName('tutoIcon')[0].getBoundingClientRect();
      c2.style.top = values3.top + 30 + 'px';
      if(values3.top + 30 < 10){
        c2.style.top = 10 + 'px';
      }
    }
    else if(position==='above') {
      let values3 = c.getElementsByClassName('tutoIcon')[0].getBoundingClientRect();
      c2.style.top = null;
      c2.style.bottom = c.clientHeight - values3.top - 40 + 'px';
    }
  }

  offset(el) {
    let rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }

  list = [
    {name:'nutrition',
      flow:[
        {name:'chatBack', action:()=>{this.globalProvider.setActiveTab(0);this.globalProvider.openChat(true)}},
        {name:'activities'},
        {name:'nutrition_select', actionAfter:()=>{this.remove()}},
        {name:'activities_window', action:()=>{this.globalProvider.openChat(false);this.globalProvider.setActiveTab(1)}},
        {name:'date_back'},
        {name:'timeframe_switcher', actionAfter:()=>{this.showTuto=0;this.remove()}},
        {name:'charts'},
        {name:'nutrition_select2', actionAfter:()=>{this.showTuto=0;this.remove()}},
        {name:'energy', actionAfter:()=>{this.showTuto=0;this.remove()}},
        {name:'progress', actionAfter:()=>{this.showTuto=0;this.remove()}},
        {name:'nutrients', actionAfter:()=>{this.showTuto=0;this.remove()}},
        {name:'water', actionAfter:()=>{this.showTuto=0;this.remove()}},
        {name:'plus_button',actionAfter:()=>{this.remove()}},
        {name:'glass', actionAfter:()=>{this.remove()}},
        {name:'selectGlass'},
        {name:'selectGlass2'},
        {name:'saveWater', actionAfter:()=>{this.showTuto=0;this.remove()}}
      ]
    },
    {name:'meal_registration',
      flow:[
        {name:'chatBack', action:()=>{this.globalProvider.setActiveTab(0);this.globalProvider.openChat(true)}},
        {name:'plus_button2', actionAfter:()=>{this.remove();}},
        {name:'nutrition_icon'},
        {name:'camera'},
        {name:'gallery'},
        {name:'gallery2'},
        {name:'image'},
        {name:'dish_choice'},
        {name:'add'},
        {name:'nutritional_infos'},
        {name:'sizes'},
        {name:'showmore'},
        {name:'saveMeal'},
        {name:'feedback'},
        {name:'image2'},
        {name:'scroller', actionAfter:()=>{this.remove()}},
        {name:'combo'},
        {name:'ingredients'},
        {name:'comboModfiy', actionAfter:()=>{this.remove()}},
        {name:'ingredients_remove'},
        {name:'ingredients_add'},
        {name:'ingredients_add2'},
        {name:'add2'},
        {name:'nutritional_infos2'},
        {name:'sizes2'},
        {name:'showmore2'},
        {name:'saveMeal2'},
        {name:'feedback2', actionAfter:()=>{this.remove();this.showTuto=0}},
      ]
    },
    {name:'intervention',
      flow:[
        {name:'chatBack', action:()=>{this.globalProvider.setActiveTab(0);this.globalProvider.openChat(true)}},
        {name:'new_activities', actionAfter:()=>{this.remove()}},
        {name:'schedule_activity'},
        {name:'select_clock'},
        {name:'schedule_activity_day'},
        {name:'schedule_activity_day_2', actionAfter:()=>{this.remove();this.showTuto=0}},
        {name:'schedule_save', actionAfter:()=>{this.remove();this.showTuto=0}},
        {name:'schedule_close'},
        {name:'activity-explanation-1'},
        {name:'activity-explanation-2',actionAfter:()=>{this.remove();this.showTuto=0}}
      ]
    },
  ];

  treatTutorial(name, e=null, subject=null, left=true, position='top', description=null, title=null){
    this.tutoTitle = name;
    if(this.tutorials && this.tutorials[name] && this.tutorials[name].activated){
      if(!this.tutorials) {
        this.tutorials = {};
      }
      this.tutorials[name] = {'activated': false};
      this.tutorialDescription = description;
      this.tutorialTitle = title;
      this.storage.set('tutorials', this.tutorials);
      if(e) {
        this.showTuto=1;
        this.showTip = this.tutorialDescription;
        this.copyElement(e, left, position, subject, name);

        let currentList = this.list.find(x=>x.name===this.tutorialC).flow;
        let el = currentList.findIndex(x => x.name === this.tutorialCurrent);
        console.log(currentList);
        console.log(el);
        if(currentList[el].action){
          currentList[el].action();
        }
      }
    }
    else{
      if(subject) {
        subject.next();
      }
    }
  }

  addTutorial(tutoName, tipName=null){
    if(!this.tutorials){
      this.tutorials={};
    }
    this.tutorialC = tutoName;
    let currentList = this.list.find(x=>x.name===tutoName).flow;
    if(tipName){
      this.tutorials[tipName] = {activated: true};
    }
    else if(currentList[0]) {
      let currentTip = currentList[0];
      this.tutorials[currentTip.name] = {activated: true};
    }
    this.storage.set('tutorials', this.tutorials);
  }

  remove(){
    this.showTip=false;
    let c = document.getElementById('canvas');
    while (c.firstChild) {
      c.removeChild(c.firstChild);
    }
  }

  finish(){
    console.log('finish');
  }
}
