import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
@Pipe({
  name: 'round'
})
export class RoundPipe implements PipeTransform {

  public transform(value: number, param: string = null): any {
    return Math.round(value);
  }

}
