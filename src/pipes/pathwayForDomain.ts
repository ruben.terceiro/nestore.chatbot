import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
@Pipe({
  name: 'pathwayForDomain'
})
export class PathwayForDomain implements PipeTransform {

  public transform(value: any, pattern: string): any {
    if(value===null){
      return null;
    }
    return value.find((x)=>x['domain-name']==pattern)['pathway-name'];
  }

}
