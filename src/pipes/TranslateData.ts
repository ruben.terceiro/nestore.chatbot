import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'translateData'})
export class TranslateDataPipe implements PipeTransform {
  transform(value: any, args: any): any {

    console.log('translateData');
    let matches = value.match(/\{{(.*?)\}}/g);
    if (matches) {
      let index = 0;
      for (let match of matches) {
        let variable = match.substring(2, match.length - 2);
        if (variable.startsWith('data.')) {
          //let attribute = variable.replace('data.', '');
          value = value.replace(new RegExp(match, 'g'), args === null ? '' : (Array.isArray(args)? args[index]: args ) );
        }
        index++;
      }
    }
    return value;
  }
}
