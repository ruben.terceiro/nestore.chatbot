import {Component} from '@angular/core';
import {GlobalvarProvider} from "../app/globalprovider";

@Component({
  selector: 'nestore-tips-welcome',
  templateUrl: 'tips-welcome.component.html',
})
export class TipsWelcomeComponent {

  constructor(private globalvarProvider: GlobalvarProvider){

  }

}
