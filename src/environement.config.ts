export const DEV = {
  NAME : 'DEV',
  COACH_SERVER: 'https://api.nestore-coach.eu/dev/hesso/coach/',
  REALM : 'development',
  API_KEY : '105_3g8b2yiyuneo04ggc40kw4gggogowok048ogcs4gooccoo08sk',
  WOT_ID : 'com.neosperience.nestoreagent.staging'

};

export const PROD = {
  NAME : 'PROD',
  COACH_SERVER: 'https://api.nestore-coach.eu/prod/hesso/coach/',
  REALM : 'nestore-coach',
  API_KEY : '104_3mqcmmjnixa8044o4g0s88o400k448o4848sw4ckw8g84oooso',
  WOT_ID : 'com.neosperience.nestoreagent'
};

export const LOCAL = {
  NAME: 'LOCAL',
  COACH_SERVER: 'http://160.98.116.216:5000/',
  REALM : DEV.REALM,
  API_KEY : DEV.API_KEY,
  WOT_ID : DEV.WOT_ID
};

export const LOCAL_PROD = {
  NAME: 'PROD',
  COACH_SERVER: LOCAL.COACH_SERVER,
  REALM : PROD.REALM,
  API_KEY : PROD.API_KEY,
  WOT_ID : PROD.WOT_ID
};

export const ENVIRONMENT = PROD;
