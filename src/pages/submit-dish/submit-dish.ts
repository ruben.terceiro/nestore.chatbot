import {Component, Input, ViewChild, Output, EventEmitter, HostListener, NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { ApiProvider } from '../../providers/api/api';
import {APIService} from "../../chat/services/api";
import {GlobalvarProvider} from "../../app/globalprovider";
import {Subject} from "rxjs";
import {TutorialService} from "../../providers/tutorialService";

@Component({
  selector: 'page-submit-dish',
  templateUrl: 'submit-dish.html',
})
export class SubmitDishPage {

  @ViewChild('nav') nav: NavController;
  @Output() onClose = new EventEmitter();
  @Output() validate = new EventEmitter();
  @Output() canBack = new EventEmitter();
  @Output() photoChange = new EventEmitter();
  @Input() selectedDish;
  @Input() type = "";
  @Input() photo = "";
  @Input() photoId = "";
  scrollTop = 0;
  error = false;
  dishDetails;
  dishHasLoaded = false;
  showAll = false;
  size = 'm';

  meal = "lunch";
  coeff = 1;
  isSubmitting = false;
  feedback;
  loading;

  openMore = new Subject();
  save = new Subject();
  feedbackSubject  = new Subject();
  feedbackSubject2  = new Subject();


  constructor(private api: APIService, public ngzone : NgZone, public globalVarProvider: GlobalvarProvider, public tutorialService: TutorialService) {
    this.openMore.subscribe(()=>{
      this.showAll = !this.showAll;
    });
    this.save.subscribe((fromTuto)=>{
      if(!fromTuto) {
        this.submitDish();
      }
      else{
        this.feedback = {"feedback":"meat"};
        this.photo = '';
        this.photoChange.emit();
      }
    });
    this.feedbackSubject.subscribe(()=>{
      this.feedback = null;
      this.close();
    });
    this.feedbackSubject2.subscribe(()=>{
      this.close();
      this.globalVarProvider.selectedDomain = null;
    });
  }

  onPageScroll(event) {
    this.ngzone.run(()=>{
      this.scrollTop = event.scrollTop;
    })
  }

  ngOnChanges(change) {
    console.log(this.type);
    if(change.selectedDish) {
      console.log(this.selectedDish);

      this.setMealTime();

      this.getDishDetails();

    }
  }

  getDishDetails() {
    this.loading = true;
    let p = this.api.postNutrientQuery({food:this.selectedDish.map(x=>x.id)});
    if(this.type=='food'){
      if(this.tutorialService.showTuto){
        let nutritionalData = {"Calcium":{"units":"mg","value":"472.36"},"Carbohydrate":{"units":"g","value":"17.11"},"Cholesterol":{"units":"mg","value":"143.64"},"DHA":{"units":"g","value":"4.05"},"EPA":{"units":"g","value":"3.2"},"Energy":{"units":"kcal","value":"282.83"},"Fat":{"units":"g","value":"22.06"},"Fiber":{"units":"g","value":"15.19"},"Folic Acid":{"units":"µg","value":"33.0"},"Iron":{"units":"mg","value":"3.12"},"Magnesium":{"units":"mg","value":"43.11"},"Mono":{"units":"g","value":"6.67"},"Poly":{"units":"g","value":"5.11"},"Protein":{"units":"g","value":"28.06"},"Saturated":{"units":"g","value":"10.1"},"Selenium":{"units":"µg","value":"24.64"},"Sodium":{"units":"mg","value":"781.5"},"Sugars":{"units":"g","value":"15.54"},"Vitamin A":{"units":"µg","value":"150.16"},"Vitamin B12":{"units":"µg","value":"11.59"},"Vitamin B6":{"units":"mg","value":"1.25"},"Vitamin C":{"units":"mg","value":"11.37"},"Vitamin D":{"units":"µg","value":"12.89"},"Vitamin E":{"units":"mg","value":"4.45"},"Zinc":{"units":"mg","value":"2.83"}};
        return this.treatDish(nutritionalData);
      }
      else{
        p = this.api.getDishDetails(this.selectedDish[0].id)
      }
    }
    else if(this.type === 'combo' && this.tutorialService.showTuto) {
      let nutritionalData = {"Calcium":{"units":"mg","value":"430.32"},"Carbohydrate":{"units":"g","value":"405.06"},"Cholesterol":{"units":"mg","value":"235.53"},"DHA":{"units":"g","value":"22.53"},"EPA":{"units":"g","value":"22.65"},"Energy":{"units":"kcal","value":"1190.77"},"Fat":{"units":"g","value":"69.05"},"Fiber":{"units":"g","value":"342.19"},"Folic Acid":{"units":"µg","value":"104.46"},"Iron":{"units":"mg","value":"354.84"},"Magnesium":{"units":"mg","value":"456.71"},"Mono":{"units":"g","value":"24.53"},"Poly":{"units":"g","value":"22.65"},"Protein":{"units":"g","value":"383.8"},"Saturated":{"units":"g","value":"13.82"},"Selenium":{"units":"µg","value":"334.89"},"Sodium":{"units":"mg","value":"2093.6"},"Sugars":{"units":"g","value":"338.06"},"Vitamin A":{"units":"µg","value":"289.31"},"Vitamin B12":{"units":"µg","value":"17.5"},"Vitamin B6":{"units":"mg","value":"13.93"},"Vitamin C":{"units":"mg","value":"38.31"},"Vitamin D":{"units":"µg","value":"20.27"},"Vitamin E":{"units":"mg","value":"15.56"},"Zinc":{"units":"mg","value":"329.82"}};
      return this.treatDish(nutritionalData);
    }
    return p.then(
      (success: Object) => {
        return this.treatDish(success)
      }
      ,
      (error) => {
        console.log(error);
        this.dishDetails = [];
        this.dishHasLoaded = true;
        this.loading=false;
        this.error = true;
        return false;
      }
    )
  }

  treatDish(success){
    if(!success['Energy']){
      this.error = true;
    }
    this.dishDetails = success;
    this.dishHasLoaded = true;
    this.loading=false;
    return true;
  }

  submitDish() {
    this.isSubmitting = true;
    // alert("calling api for dish: " + dish);
    let obj = this.selectedDish;
    let text;
    if(this.selectedDish.length==1){
      obj = this.selectedDish[0].id;
      text= this.selectedDish[0].text;
    }
    else{
      obj=this.selectedDish.map(x=>x.id);
      text=this.selectedDish.map(x=>x.text).join(', ');
    }
    this.api.sendDishFromName(obj, text, this.type, this.size, this.photoId).then(
      (success) => {
        this.isSubmitting = false;
        this.feedback=success;
        console.log(success);
        this.globalVarProvider.reloadActivities();
      },
      (error) => {
        this.isSubmitting = false;
        this.globalVarProvider.internalError = true;
      }
    );
    // close();
  }

  close() {
    this.onClose.emit()
  }

  setMealTime() {
    const now = new Date();

    var breakfast = new Date();
    breakfast.setHours(5);
    var lunch = new Date();
    lunch.setHours(11);
    var dinner = new Date();
    dinner.setHours(17);

    if(now.getTime() >= breakfast.getTime() && now.getTime() < lunch.getTime()) {
      this.meal = "breakfast";
    } else if(now.getTime() >= lunch.getTime() && now.getTime() < dinner.getTime()) {
      this.meal = "lunch";
    } else {
      this.meal = "dinner";
    }
  }
}
