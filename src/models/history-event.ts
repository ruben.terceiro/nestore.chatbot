export abstract class HistoryEvent {

    constructor(
        public domain: string,
        public timestamp: Date,
        public ts_str: string,
        public id: string,
    ) {
    }
}
