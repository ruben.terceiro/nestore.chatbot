export class Threshold {
    percentages: Object;
    inRange: boolean;
    hasUpper: boolean;
    hasData: boolean;

    constructor(
        public nutrient: string,
        public lower: number,
        public upper: number,
        public value: number = 0,
        public unit: string,
    ) {
        this.hasData = !(value === undefined || value === null || isNaN(value)
            || lower === undefined || lower === null || isNaN(lower));
        this.hasUpper = !(upper === undefined || upper === null || isNaN(upper));
        // value in range or not?
        this.inRange = this.hasUpper ? (value >= lower && value <= upper) : (value >= lower);
        this.calcPercentages();
     }

    private calcPercentages() {
        // if lower and upper are present, fix upper to 80%
        // if only lower is availabe, set it to 50%
        const UPPER_PERC = 80;
        const LOWER_PERC = 50;

        let perc = (x: number) => this.hasUpper ? UPPER_PERC * x / this.upper : LOWER_PERC * x / this.lower

        this.percentages = {
            min: 0,
            max: 100,
            lower: perc(this.lower),
            upper: this.hasUpper ? perc(this.upper) : 100,
            value: perc(this.value) > 100 ? 100 : perc(this.value),
            goodWidth: perc(this.upper) - perc(this.lower),
        };
    }
}
