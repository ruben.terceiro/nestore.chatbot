export class Summary {
    constructor(
        public nutrient: String,
        public value: number
    ) { }
}