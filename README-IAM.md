# IAM Integration

## Overview

Integration and access to keycloak services is done through `KeycloakService` (see `src/services/keycloak.service.ts`). It exposes methods for:

- login
- logout
- get user information
- get roles
- check if the user has a specific role

and a couple of others...

The service is initialize during application bootstrap (see `src/app/main.ts`) and then it's used to populate a sample user tab (`src/pages/user`). The tab displays the current user id (press on the fingerprint button), name, email and a few other attributes (if they are added to the user in keycloak admin page).

The login is redirected toward the standard keycloak web login. All the functions available for the web login can be used from the mobile application.

## Configuration

The configuration for the IAM can be found (and modified) in `src/config/keycloak.json`. The important settings are:

- realm: it's the name of the realm to be used; currently it's set as `development`
- clientId: it's the id of OpenId Connect client created in the keycloak realm.

If you change any of the values please synchronize them with the setup done in Keycloak Console. Integration with another compatible IAM system can be done by modifying the json file.

## Usage

Sample service invokation can be seen in the `src/pages/user/user.ts` file. The most interesting methods are `loadUserProfile`, `loadUserInfo` and `viewGuard`. The first 2 load information related to the user (user name, email, attributes, roles, id). The `viewGuard` just checks if the neede role (parameter) is among the ones assigned to the user. It can be used to limit access in certain pages to users having special roles.

The `loadUserProfile` retrieves a very small set of standard information and attributes, please do not be mislead by the name to think it's the full user profile (as defined in Nestore project). It's related strictly to IAM. The Nestore's user profile is another topic and it's not defined here (IAM and this document).

The `userId` as returned by `loadUserInfo` should be the main `id` to query external services in the context of the current user (i.e. to get the Nestore profile when it would become available).

### Register

Registering a new user can be done from the login screen or directly from admin console (see below). From login screen you have to press `Register`, complete the fields, wait for the validation email and proceed from there.

### Login

Login is available once an account is registered and validated.

### Forgot password

If you forgot the password provided during registration flow you can reset it by pressing on `Forgot Password` link.

## Keycloak Console

The Development realm admin console can be accessed at [https://iam.nestore-coach.eu/auth/admin/](https://iam.nestore-coach.eu/auth/admin/) by using `admin-dev` account.

The registered users list can be seen in [Users](https://iam.nestore-coach.eu/auth/admin/master/console/#/realms/development/users) section (press `View all users` first time when the link is open). By clicking on the user id the full information is display and can be amended. You might want to add custom attributes (`Attributes` tab) and assign `Roles` (at least `user:read` to have access to mobile app sample user tab).

Adding a new user can be done directly in the console, if needed (but we recommand using `Register` from Login screen).

Modifying users and realm settings (add new roles, register new applications/clients) should be done from this console.

### Roles

Currently defined roles in Development realm can be seen at [Roles](https://iam.nestore-coach.eu/auth/admin/master/console/#/realms/development/roles). New ones can be added and the existing ones removed if not needed anymore. Then the roles can be assigned to users and can be verified in applications.